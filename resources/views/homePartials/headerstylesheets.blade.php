
    <title>Morteza Jelokhani @yield('title') Portfolio </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{asset('frontend/images/icons/favicon.png')}}"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/hero-slider.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/templatemo-main.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/owl-carousel.css')}}">
    <!--===============================================================================================-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <!--===============================================================================================-->
    <style>

        @media (min-width: 600px) {
            .title-info-vertical-section {
                transform: rotate(90deg);
                font-size: 30px!important;
                transform-origin: left top 0;
                display: inline-block;
                position: absolute;
                color: #cc9966;
            }
        }
        .title-vertical > div {

            text-align: center;
            font-size: 30px;
        }
        .title-info-vertical-section {

            display: block!important;
            margin-bottom: 20px!important;
            transform-origin: left top 0;
            color: #ff7e00;


            left: 70px;
        }

        .title-font{
            font-family: 'Open Sans', sans-serif;
            font-size: 20px;
            text-shadow: 4px 4px 4px #aaa;
        }

        .glow {
            color: #fff;
            text-align: center;
            -webkit-animation: glow 1s ease-in-out infinite alternate;
            -moz-animation: glow 1s ease-in-out infinite alternate;
            animation: glow 1s ease-in-out infinite alternate;
        }
        @media (max-width: 767px){
            title-info-vertical-section {
                transform: rotate(180deg);
                transform-origin: left top 0;
                color: #ff7e00;
                display: inline-block;
                position: absolute;
                left: 70px;
            }

        }
        @-webkit-keyframes glow {
            from {
                text-shadow: 0 0 10px rgba(255, 126, 0, 0.46), 0 0 20px rgba(255, 126, 0, 0.87), 0 0 30px rgba(255, 126, 0, 0.49), 0 0 40px #ff7d00, 0 0 50px rgba(255, 126, 0, 0.64), 0 0 60px #bf5c00, 0 0 70px rgba(246, 131, 22, 0.78);
            }

            to {
                text-shadow: 0 0 20px #ff7000, 0 0 30px #ff7b00, 0 0 40px #ff7b00, 0 0 50px #ff7b00, 0 0 60px #ff4da6, 0 0 70px #ff7b00, 0 0 80px #ff7b00;
            }
        }




    </style>

